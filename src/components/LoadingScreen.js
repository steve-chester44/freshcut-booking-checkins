import React from "react"

import styled from "styled-components"

const Container = styled("div")`
	width: 100%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
`

export default () => (
	<Container>
		<h1>Loading...</h1>
	</Container>
)
