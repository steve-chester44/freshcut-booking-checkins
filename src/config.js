export default {
	SUBSCRIPTION_URI: process.env.REACT_APP_SUBSCRIPTION_URI,
	API_URL: process.env.REACT_APP_API_URL
}
